<!DOCTYPE html>
<html lang="es">
<head>
    <body>
        <link rel="stylesheet" type="text/css" href="./css/font-awesome-4.7.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="./js/jquery-ui-1.12.1/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="./js/primeui-4.1.15/primeui.css">
        <link rel="stylesheet" type="text/css" href="./js/primeui-4.1.15/themes/delta/theme.css?version=1.1">
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <script type="text/javascript" src="./js/jquery.js?version=3.1.11"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js?version=1.211"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.12.1/jquery-ui.min.js?version=1.12.11"></script>
        <script type="text/javascript" src="./js/primeui-4.1.15/primeui.js?version=2.5"></script>
        <script type="text/javascript" src="./js/primeui-4.1.15/x-tag-core.min.js"></script>
        <script type="text/javascript" src="./js/primeui-4.1.15/primeelements.min.js"></script>
    </body>
    <script>
        var globalData = []
        var dataHombres = []
        var dataMujeres = []
        var registros = 100
        function getDataRest(){
            console.log('obteniendo...')
            registros = $("#registros").val()//()
            console.log(registros)
            $.ajax({
                type: 'GET',
                url: 'https://randomuser.me/api/?results='+registros,
                dataType: 'json',
                async: true,
                success: (rta) => {
                    console.log(rta)
                    globalData = rta.results
                    console.log(this.globalData)
                    dataHombres = globalData.filter( person =>{
                        return person.gender == 'male'
                    });
                    dataHombres.sort((a,b) => {
                        a.fullname = a.name.title + ' ' + a.name.first + ' ' + a.name.last
                        b.fullname = b.name.title + ' ' + b.name.first + ' ' + b.name.last
                        a.age = a.dob.age
                        b.age = b.dob.age
                        return a.dob.age - b.dob.age
                    });
                    dataMujeres = globalData.filter(person=>{
                        return person.gender == 'female'
                    });
                    dataMujeres.sort((a, b) => {
                        a.fullname = a.name.title + ' ' + a.name.first + ' ' + a.name.last
                        b.fullname = b.name.title + ' ' + b.name.first + ' ' + b.name.last
                        a.age = a.dob.age
                        b.age = b.dob.age
                        if(a.email.toLowerCase() < b.email.toLowerCase()) { 
                        return 1;
                        }
                        if(a.email.toLowerCase() > b.email.toLowerCase()) { 
                        return -1;
                        }
                        return 0;
                    });
                    createTable('tblHombres', "Hombres (" + dataHombres.length + ")", dataHombres);
                    createTable('tblMujeres', "Mujeres (" + dataMujeres.length + ")", dataMujeres);
                },error: (xhr) => {
                    console.log(xhr)
                }
            })
        }
        function createTable($id, $title, $data){
            $("#"+$id).replaceWith("<div id='"+$id+"'></div>")
            $("#"+$id).puidatatable({
                caption: $title,
                datasource: $data,
                scrollable: true,
                scrollHeight: '300',
                columns: [
                    {field: 'fullname',headerText: 'Nombres', sortable: true,content:function(row){
                        return row.name.title + ' ' + row.name.first + ' ' + row.name.last
                    }},
                    {field: 'gender', headerStyle:"width: 120px",headerText: 'Genero', sortable: false},
                    {field: 'age', headerStyle:"width: 80px",headerText: 'Edad', sortable: true,content:function(row){
                        return row.age
                    }},
                    {field: 'email', headerText: 'Email', sortable: true}
                    
                ]
            });
        }
        $(document).ready(function(){
            $("#registros").puidropdown({change:function(event){
                getDataRest();
            }})
            getDataRest();
        })
    </script>
    <style>
        .mb-2{
            margin-bottom: .4rem;
        }
        .mt-2{
            margin-top:.4rem;
        }
        
        .flex-center{
            display:flex;
            justify-content: center;
        }
        .select-registros{
            width:240px;
        }
        .ui-corner-right{
            padding: 0px !important;
        }
    </style>
</head>
<body>
    <div class="row">
        <div class="col-lg-12 col-12 mt-2 mb-2 flex-center">
            <select id="registros" onchange="getDataRest()" class="select-registros">
                <option value="10">10 Registros</option>
                <option value="25">25 Registros</option>
                <option value="50">50 Registros</option>
                <option value="75">75 Registros</option>
                <option value="100" selected>100 Registros</option>
                <option value="1000">1000 Registros</option>
            </select>
        </div>
        <div class="col-lg-6 col-sm-12 col-12">
            <div id="tblHombres"></div>
        </div>
        <div class="col-lg-6 col-sm-12 col-12">
            <div id="tblMujeres"></div>
        </div>
    </div>
</body>
</html>